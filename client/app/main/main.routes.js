'use strict'

export default function routes($stateProvider) {
  'ngInject'

  $stateProvider.state('main', {
    url: '/',
    template: '<main></main>'
  })
    .state('product', {
    resolve: {
      renderProduct: function($http, $stateParams){
        $http.get('/api/things/'+$stateParams.productId)
          .then(response => {
            console.log('response.data--', response.data)
            return response.data
          }).catch(err => console.log('err--', err))
      }
    },

    url: '/product/:productId',
    template: require('../product-view/product.pug')
  })

}
