import angular from 'angular'
import uiRouter from 'angular-ui-router'
import routing from './product.routes'

export class productController {
  $http
  socket
  renderProduct
  awesomeThings = []
  newThing = ''

  /*@ngInject*/
  constructor($http, $scope, socket, renderProduct) {
    this.$http = $http
    this.socket = socket
    this.renderProduct = $scope.renderProduct

    $scope.$on('$destroy', function () {
      socket.unsyncUpdates('thing')
    })

  }

  $onInit() {
    console.log('this.renderProduct--', this.renderProduct)
    // console.log('$state.params--', $state.params)
    // this.$http.get('/api/things/')
    //   .then(response => {
    //     this.awesomeThings = response.data
    //     this.socket.syncUpdates('thing', this.awesomeThings)
    //   })
  }

  addThing() {
    if (this.newThing) {
      this.$http.post('/api/things', {
        name: this.newThing
      })
      this.newThing = ''
    }
  }

  deleteThing(thing) {
    this.$http.delete(`/api/things/${thing._id}`)
  }
}

export default angular.module('angularBorisovApp.product', [uiRouter])
  .config(routing)
  .component('product', {
    template: require('./product.pug'),
    controller: productController
  })
  .name
