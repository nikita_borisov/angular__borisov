'use strict';

import product from './product.component';
import {
  productController
} from './product.component';

describe('Component: productComponent', function() {
  beforeEach(angular.mock.module(product));
  beforeEach(angular.mock.module('stateMock'));
  beforeEach(angular.mock.module('socketMock'));

  var scope;
  var productComponent;
  var state;
  var $httpBackend;

  // Initialize the controller and a mock scope
  beforeEach(inject(function(_$httpBackend_, $http, $componentController, $rootScope, $state,
    socket) {
    $httpBackend = _$httpBackend_;
    $httpBackend.expectGET('/api/things')
      .respond(['HTML5 Boilerplate', 'AngularJS', 'Karma', 'Express']);

    scope = $rootScope.$new();
    state = $state;
    productComponent = $componentController('product', {
      $http,
      $scope: scope,
      socket
    });
  }));

  it('should attach a list of things to the controller', function() {
    productComponent.$onInit();
    $httpBackend.flush();
    expect(productComponent.awesomeThings.length)
      .to.equal(4);
  });
});
