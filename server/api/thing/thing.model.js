'use strict'

import mongoose from 'mongoose'
import {registerEvents} from './thing.events'

const ThingSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean,
  image: String,
  producer: String,
  weight: Number,
  price: Number
})

registerEvents(ThingSchema)
export default mongoose.model('Thing', ThingSchema)
